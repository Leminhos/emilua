local sleep_for = require('sleep_for')

this_fiber.forbid_suspend()
sleep_for(1)
